#!/usr/bin/gnuplot

set xrange[0:201]
set yrange[0:26]
# set y2range[0:100]

# set ytics 40 nomirror tc lt 1
# set y2tics 20 nomirror tc lt 2
unset key
set cbrange [40:80]
load 'RdBu.plt'

set title 'APD(DI)'
set palette negative
#set xrange [0:2500]
#set yrange [0:159]
set xlabel 'DI (ms)'
set ylabel 'D_2'
set xtics ("100" 5, "250" 90, "400" 180)
set ytics ("1.3" 0, "1.65" 13, "1.95" 26 )


set terminal eps enhanced font "DejaVuSans,20"
set output 'fig6_1.eps'
plot "fig6_1.bin"  binary array=(201,27) scan=yx format="%double" with image
