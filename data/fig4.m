#!/usr/bin/octave
clear all;

h=figure(1);
fd=fopen('chain_30_100_30_time5000ms.bin','r');
x=fread(fd, [160 2500], 'double');
imagesc(x, [-80 20]);

set(gca(),'outerposition',[0.05,0.05,0.9,0.9]);
set(gca(),'xtick', [1 1250 2500]);
set(gca(),'xticklabel', [0 2500 5000]);
xlabel('time, ms')
set(gca(),'ytick', [1 31 130 160]);
set(gca(),'yticklabel', [1 31  131 160 ]);
ylabel('i')

W = 7; H = 7;
set(h,'PaperUnits','inches')
set(h,'PaperOrientation','landscape');
set(h,'PaperSize',[H,W])
set(h,'PaperPosition',[0,0,W,H])
FN = findall(h,'-property','FontName');
set(FN,'FontName','Helvetica');
FS = findall(h,'-property','FontSize');
set(FS,'FontSize',14);
colorbar;
print (h,'-dpdf','fig4.pdf')
%print -dpng fig4.png');
% print('fig4.pdf');
